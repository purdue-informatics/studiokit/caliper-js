module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    babel: {
      options: {
        plugins: ['./esmSafeRequireTransform.js'],
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'src/',
          src: ['**/*.js'],
          dest: 'dist',
        }]
      }
    },
    'http-server': {
      'dev': {
        // the server root directory
        root: '.',
        port: 8888,
        host: "127.0.0.1",
        cache: 0,
        showDir: true,
        autoIndex: true,
        // server default file extension
        ext: "html",
        // run in parallel with other tasks
        runInBackground: false
      }
    },
    tape: {
      options: {
        pretty: true,
        output: 'console'
      },
      files: ['test/**/*.js']
    },
    jsdoc: {
      dist: {
        src: ['src/**/*.js', 'README-external.md'],
        options: {
          destination: 'doc'
        }
      }
    },
    clean: {
      dist: 'dist/**'
    }
  });

  // Load the plugin and tasks that provides unit testing via tap/tape
  grunt.loadNpmTasks('grunt-tape');
  grunt.registerTask('test', ['tape']);
  grunt.registerTask('ci', ['tape:ci']);

  // Load the plugin that provides the "babel" task.
  grunt.loadNpmTasks('grunt-babel');

    // Load the plugin that provides the "clean" task.
  grunt.loadNpmTasks('grunt-contrib-clean')

  // Load plugin that provides http-server task
  grunt.loadNpmTasks('grunt-http-server');

  // Load jsDoc3 plugin
  grunt.loadNpmTasks('grunt-jsdoc');

  // Default task(s).
  grunt.registerTask('default', ['test', 'clean', 'babel', 'jsdoc']);
  grunt.registerTask('server', ['http-server']);
};
