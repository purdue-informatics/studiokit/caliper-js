module.exports = function(babel) {
    const { types: t } = babel;

    let requireExists = false;
    return {
        name: "esm-safe-require",
        visitor: {
            VariableDeclaration(path) {
                const declarations = path.node.declarations;
                for (const declaration of declarations) {
                    if (
                        t.isCallExpression(declaration.init) &&
                        declaration.init.callee.name === 'require' &&
                        !!declaration.init.arguments &&
                        t.isStringLiteral(declaration.init.arguments[0]) &&
                        !declaration.init.arguments[0].value.startsWith('.')
                    ) {
                        requireExists = true;
                        const requireCall = t.callExpression(
                            t.identifier("require"),
                            [declaration.init.arguments[0]]
                        );
                        const esmSafeRequireCall = t.callExpression(
                            t.identifier("__esmSafeRequire"),
                            [requireCall]
                        );
                        const variableDeclarator = t.variableDeclarator(declaration.id, esmSafeRequireCall);
                        path.replaceWith(t.variableDeclaration(path.node.kind, [variableDeclarator]));
                    }
                }
            },
            Program: {
                exit(path) {
                    if (requireExists) {
                        requireExists = false;
                        const func = t.functionDeclaration(
                            t.identifier("__esmSafeRequire"),
                            [t.identifier("module")],
                            t.blockStatement([
                                t.ifStatement(
                                    t.memberExpression(
                                        t.identifier("module"),
                                        t.identifier("__esModule")
                                    ),
                                    t.returnStatement(
                                        t.memberExpression(
                                            t.identifier("module"),
                                            t.identifier("default")
                                        )
                                    ),
                                    t.returnStatement(t.identifier("module"))
                                )
                            ])
                        );
                        path.unshiftContainer("body", func);
                    }
                }
            }
        }
    };
};
